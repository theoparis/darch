package stage

import (
	"git.flowtr.dev/theoparis/darch/pkg/cmd/darch/commands"
	"git.flowtr.dev/theoparis/darch/pkg/reference"
	"git.flowtr.dev/theoparis/darch/pkg/staging"
	"github.com/urfave/cli/v2"
)

var removeCommand = cli.Command{
	Name:      "remove",
	Usage:     "removes an image from the stage",
	ArgsUsage: "<image[:tag]>",
	Action: func(clicontext *cli.Context) error {
		var (
			imageName = clicontext.Args().First()
		)

		err := commands.CheckForRoot()
		if err != nil {
			return err
		}

		imageRef, err := reference.ParseImage(imageName)
		if err != nil {
			return err
		}

		stagingSession, err := staging.NewSession()
		if err != nil {
			return err
		}

		err = stagingSession.Remove(imageRef)
		if err != nil {
			return err
		}

		return stagingSession.SyncBootloader()
	},
}
